package com.itv.supermarket.service

import com.itv.supermarket.model.Rule
import com.itv.supermarket.repository.ItemRepository
import com.itv.supermarket.repository.RuleRepository
import spock.lang.Specification

class ShoppingCartServiceImplTest extends Specification {

    def itemRepository;
    def ruleRepository;

    def setup() {
        itemRepository = Mock(ItemRepository)
        ruleRepository = Mock(RuleRepository)
    }

    def "Shopping cart checkout with empty cart"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        when: "Shopping cart checkout"
        def receiptItems = shoppingCartService.checkout()

        then: "No receipt items are returned"
        receiptItems.isEmpty()
    }

    def "Shopping cart checkout with no valid items in item repository"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice(_) >> Optional.empty()
        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("C")
        shoppingCartService.addToCart("B")

        and: "Shopping cart checkout"
        def results = shoppingCartService.checkout()

        then: "Receipt items returned correctly"
        results.size() == 0
    }

    def "Shopping cart checkout with no rules"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice("A") >> Optional.of(BigDecimal.valueOf(0.50))
        itemRepository.getItemPrice("B") >> Optional.of(BigDecimal.valueOf(0.30))
        itemRepository.getItemPrice("C") >> Optional.of(BigDecimal.valueOf(0.20))
        itemRepository.getItemPrice("D") >> Optional.of(BigDecimal.valueOf(0.15))

        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("C")
        shoppingCartService.addToCart("B")

        and: "Shopping cart checkout"
        def results = shoppingCartService.checkout()

        then: "Receipt items sorted and returned correctly"
        results.size() == 3

        "A".equals(results[0].getSku())
        2.equals(results[0].getQty())
        BigDecimal.valueOf(1.00).compareTo(results[0].getItemPrice()) == 0
        BigDecimal.valueOf(1.00).compareTo(results[0].getReceiptPrice()) == 0

        "B".equals(results[1].getSku())
        1.equals(results[1].getQty())
        BigDecimal.valueOf(0.30).compareTo(results[1].getItemPrice()) == 0
        BigDecimal.valueOf(0.30).compareTo(results[1].getReceiptPrice()) == 0

        "C".equals(results[2].getSku())
        1.equals(results[2].getQty())
        BigDecimal.valueOf(0.20).compareTo(results[2].getItemPrice()) == 0
        BigDecimal.valueOf(0.20).compareTo(results[2].getReceiptPrice()) == 0

    }

    def "Shopping cart checkout where the rules do not match"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice("A") >> Optional.of(BigDecimal.valueOf(0.50))
        itemRepository.getItemPrice("B") >> Optional.of(BigDecimal.valueOf(0.30))
        itemRepository.getItemPrice("C") >> Optional.of(BigDecimal.valueOf(0.20))
        itemRepository.getItemPrice("D") >> Optional.of(BigDecimal.valueOf(0.15))

        ruleRepository.getRule("E") >> Optional.of(new Rule(3, BigDecimal.valueOf(1.30)))
        ruleRepository.getRule("F") >> Optional.of(new Rule(2, BigDecimal.valueOf(0.45)))

        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("D")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("C")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("A")

        and: "Shopping cart checkout"
        def results = shoppingCartService.checkout()

        then: "Receipt items sorted and returned correctly"
        results.size() == 4

        "A".equals(results[0].getSku())
        4.equals(results[0].getQty())
        BigDecimal.valueOf(2.00).compareTo(results[0].getItemPrice()) == 0
        BigDecimal.valueOf(2.00).compareTo(results[0].getReceiptPrice()) == 0

        "B".equals(results[1].getSku())
        2.equals(results[1].getQty())
        BigDecimal.valueOf(0.60).compareTo(results[1].getItemPrice()) == 0
        BigDecimal.valueOf(0.60).compareTo(results[1].getReceiptPrice()) == 0

        "C".equals(results[2].getSku())
        1.equals(results[2].getQty())
        BigDecimal.valueOf(0.20).compareTo(results[2].getItemPrice()) == 0
        BigDecimal.valueOf(0.20).compareTo(results[2].getReceiptPrice()) == 0

        "D".equals(results[3].getSku())
        1.equals(results[3].getQty())
        BigDecimal.valueOf(0.15).compareTo(results[3].getItemPrice()) == 0
        BigDecimal.valueOf(0.15).compareTo(results[3].getReceiptPrice()) == 0

    }

    def "Shopping cart checkout with rules"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice("A") >> Optional.of(BigDecimal.valueOf(0.50))
        itemRepository.getItemPrice("B") >> Optional.of(BigDecimal.valueOf(0.30))
        itemRepository.getItemPrice("C") >> Optional.of(BigDecimal.valueOf(0.20))
        itemRepository.getItemPrice("D") >> Optional.of(BigDecimal.valueOf(0.15))

        ruleRepository.getRule("A") >> Optional.of(new Rule(3, BigDecimal.valueOf(1.30)))
        ruleRepository.getRule("B") >> Optional.of(new Rule(2, BigDecimal.valueOf(0.45)))

        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("D")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("C")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("A")

        and: "Shopping cart checkout"
        def results = shoppingCartService.checkout()

        then: "Receipt items sorted and returned correctly"
        results.size() == 4

        "A".equals(results[0].getSku())
        4.equals(results[0].getQty())
        BigDecimal.valueOf(2.00).compareTo(results[0].getItemPrice()) == 0
        BigDecimal.valueOf(1.80).compareTo(results[0].getReceiptPrice()) == 0

        "B".equals(results[1].getSku())
        2.equals(results[1].getQty())
        BigDecimal.valueOf(0.60).compareTo(results[1].getItemPrice()) == 0
        BigDecimal.valueOf(0.45).compareTo(results[1].getReceiptPrice()) == 0

        "C".equals(results[2].getSku())
        1.equals(results[2].getQty())
        BigDecimal.valueOf(0.20).compareTo(results[2].getItemPrice()) == 0
        BigDecimal.valueOf(0.20).compareTo(results[2].getReceiptPrice()) == 0

        "D".equals(results[3].getSku())
        1.equals(results[3].getQty())
        BigDecimal.valueOf(0.15).compareTo(results[3].getItemPrice()) == 0
        BigDecimal.valueOf(0.15).compareTo(results[3].getReceiptPrice()) == 0

    }

    def "Shopping cart checkout can apply the same rule multiple times"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice("A") >> Optional.of(BigDecimal.valueOf(0.50))
        itemRepository.getItemPrice("B") >> Optional.of(BigDecimal.valueOf(0.30))

        ruleRepository.getRule("A") >> Optional.of(new Rule(3, BigDecimal.valueOf(1.30)))
        ruleRepository.getRule("B") >> Optional.of(new Rule(2, BigDecimal.valueOf(0.45)))

        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")

        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("B")
        shoppingCartService.addToCart("B")

        and: "Shopping cart checkout"
        def results = shoppingCartService.checkout()

        then: "Receipt items sorted and returned correctly"
        results.size() == 2

        "A".equals(results[0].getSku())
        10.equals(results[0].getQty())
        BigDecimal.valueOf(5.00).compareTo(results[0].getItemPrice()) == 0
        BigDecimal.valueOf(4.40).compareTo(results[0].getReceiptPrice()) == 0

        "B".equals(results[1].getSku())
        4.equals(results[1].getQty())
        BigDecimal.valueOf(1.20).compareTo((results[1].getItemPrice())) == 0
        BigDecimal.valueOf(0.90).compareTo(results[1].getReceiptPrice()) == 0

    }

    def "Shopping cart is emptied successfully"() {
        given: "Shopping cart service is created"
        def shoppingCartService = new ShoppingCartServiceImpl(itemRepository, ruleRepository)

        and: "Mock items setup"
        itemRepository.getItemPrice("A") >> Optional.of(BigDecimal.valueOf(0.50))

        ruleRepository.getRule("A") >> Optional.of(new Rule(3, BigDecimal.valueOf(1.30)))

        ruleRepository.getRule(_) >> Optional.empty()

        when: "Items added to shopping cart"
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")
        shoppingCartService.addToCart("A")

        and: "Shopping cart is emptied"
        shoppingCartService.emptyCart()

        and:
        def results = shoppingCartService.checkout()

        then: "No receipt items are returned"
        results.size() == 0

    }




}
