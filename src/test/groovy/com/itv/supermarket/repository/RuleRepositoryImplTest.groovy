package com.itv.supermarket.repository

import com.itv.supermarket.repository.exception.RepositoryException
import spock.lang.Specification

class RuleRepositoryImplTest extends Specification {

    def "getRule returns empty optional when sku not found"() {
        given: "A new rule repository is created"
        def ruleRepository = new RuleRepositoryImpl()

        when: "Get a rule that does not exist"
        def rule = ruleRepository.getRule("A")

        then: "An empty optional is returned"
        !rule.isPresent()
    }

    def "An exception is thrown when loading a csv file that does not exist"() {
        given: "A new rule repository is created"
        def ruleRepository = new RuleRepositoryImpl()

        when: "Rule repository is loaded with an invalid file"
        ruleRepository.load("")

        then: "An exception is thrown"
        thrown(RepositoryException)
    }

    def "An exception is thrown when loading a csv file with an invalid special price"() {
        given: "A new rule repository is created"
        def ruleRepository = new RuleRepositoryImpl()

        when: "Rule repository is loaded with an invalid file"
        ruleRepository.load("src/test/resources/rules_invalid1.csv")

        then: "An exception is thrown"
        thrown(RepositoryException)
    }

    def "Invalid lines in the csv file are ignored"() {
        given: "A new rule repository is created"
        def ruleRepository = new RuleRepositoryImpl()

        when: "Rule repository is loaded with an invalid file"
        ruleRepository.load("src/test/resources/rules_invalid2.csv")

        then: "Only rules with valid lines are loaded"
        !ruleRepository.getRule("A").isPresent()
        !ruleRepository.getRule("B").isPresent()
        !ruleRepository.getRule("C").isPresent()
        ruleRepository.getRule("D").isPresent()
    }

    def "Rule CSV file is loaded successfully"() {
        given: "A rule item repository is created"
        def ruleRepository = new RuleRepositoryImpl()

        when: "Repository is loaded from csv file"
        ruleRepository.load("src/test/resources/rules.csv")

        then: "Rule are loaded successfully"
        def ruleA = ruleRepository.getRule("A").get()
        3.equals(ruleA.getQty())
        BigDecimal.valueOf(1.30).compareTo(ruleA.getSpecialPrice()) == 0

        def ruleB = ruleRepository.getRule("B").get()
        2.equals(ruleB.getQty())
        BigDecimal.valueOf(0.45).compareTo(ruleB.getSpecialPrice()) == 0
    }

}
