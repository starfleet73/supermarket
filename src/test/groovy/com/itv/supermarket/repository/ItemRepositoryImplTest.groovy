package com.itv.supermarket.repository

import com.itv.supermarket.repository.exception.RepositoryException
import spock.lang.Specification

class ItemRepositoryImplTest extends Specification {

    def "getItemPrice returns empty optional when item sku not found"() {
        given: "A new item repository is created"
        def itemRepository = new ItemRepositoryImpl()

        when: "Get an item price that does not exist"
        def itemPrice = itemRepository.getItemPrice("A")

        then: "An empty optional is returned"
        !itemPrice.isPresent()
    }

    def "An exception is thrown when loading a csv file that does not exist"() {
        given: "A new item repository is created"
        def itemRepository = new ItemRepositoryImpl()

        when: "Item repository is loaded with an invalid file"
        itemRepository.load("")

        then: "An exception is thrown"
        thrown(RepositoryException)
    }

    def "An exception is thrown when loading a csv file with an invalid unit price"() {
        given: "A new item repository is created"
        def itemRepository = new ItemRepositoryImpl()

        when: "Item repository is loaded with an invalid file"
        itemRepository.load("src/test/resources/items_invalid1.csv")

        then: "An exception is thrown"
        thrown(RepositoryException)
    }

    def "Invalid lines in the csv file are ignored"() {
        given: "A new item repository is created"
        def itemRepository = new ItemRepositoryImpl()

        when: "Item repository is loaded with an invalid file"
        itemRepository.load("src/test/resources/items_invalid2.csv")

        then: "Only Items with valid lines are loaded"
        !itemRepository.getItemPrice("A").isPresent()
        !itemRepository.getItemPrice("B").isPresent()
        itemRepository.getItemPrice("C").isPresent()
        !itemRepository.getItemPrice("D").isPresent()
    }

    def "Item CSV file is loaded successfully"() {
        given: "A new item repository is created"
        def itemRepository = new ItemRepositoryImpl()

        when: "Repository is loaded from csv file"
        itemRepository.load("src/test/resources/items.csv")

        then: "Items are loaded successfully"
        BigDecimal.valueOf(0.50).compareTo(itemRepository.getItemPrice("A").get()) == 0
        BigDecimal.valueOf(0.30).compareTo(itemRepository.getItemPrice("B").get()) == 0
        BigDecimal.valueOf(0.20).compareTo(itemRepository.getItemPrice("C").get()) == 0
        BigDecimal.valueOf(0.15).compareTo(itemRepository.getItemPrice("D").get()) == 0

    }

}
