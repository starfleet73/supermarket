package com.itv.supermarket.model;

import java.math.BigDecimal;

public class Rule {
    private Integer qty;
    private BigDecimal specialPrice;

    public Rule(Integer qty, BigDecimal specialPrice) {
        this.qty = qty;
        this.specialPrice = specialPrice;
    }

    public Integer getQty() {
        return qty;
    }

    public BigDecimal getSpecialPrice() {
        return specialPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rule rule = (Rule) o;

        if (qty != null ? !qty.equals(rule.qty) : rule.qty != null) return false;
        return specialPrice != null ? specialPrice.equals(rule.specialPrice) : rule.specialPrice == null;
    }

    @Override
    public int hashCode() {
        int result = qty != null ? qty.hashCode() : 0;
        result = 31 * result + (specialPrice != null ? specialPrice.hashCode() : 0);
        return result;
    }

}
