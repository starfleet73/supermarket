package com.itv.supermarket.model;

import java.math.BigDecimal;

public class ReceiptItem {
    private String sku;
    private Integer qty;
    private BigDecimal itemPrice;
    private BigDecimal receiptPrice;

    public ReceiptItem(String sku, Integer qty, BigDecimal itemPrice, BigDecimal receiptPrice) {
        this.sku = sku;
        this.qty = qty;
        this.itemPrice = itemPrice;
        this.receiptPrice = receiptPrice;
    }

    public String getSku() {
        return sku;
    }

    public Integer getQty() {
        return qty;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public BigDecimal getReceiptPrice() {
        return receiptPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReceiptItem that = (ReceiptItem) o;

        if (sku != null ? !sku.equals(that.sku) : that.sku != null) return false;
        if (qty != null ? !qty.equals(that.qty) : that.qty != null) return false;
        if (itemPrice != null ? !itemPrice.equals(that.itemPrice) : that.itemPrice != null) return false;
        return receiptPrice != null ? receiptPrice.equals(that.receiptPrice) : that.receiptPrice == null;
    }

    @Override
    public int hashCode() {
        int result = sku != null ? sku.hashCode() : 0;
        result = 31 * result + (qty != null ? qty.hashCode() : 0);
        result = 31 * result + (itemPrice != null ? itemPrice.hashCode() : 0);
        result = 31 * result + (receiptPrice != null ? receiptPrice.hashCode() : 0);
        return result;
    }

}
