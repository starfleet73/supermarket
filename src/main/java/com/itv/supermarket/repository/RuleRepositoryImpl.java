package com.itv.supermarket.repository;

import com.itv.supermarket.model.Rule;
import com.itv.supermarket.repository.exception.RepositoryException;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class RuleRepositoryImpl implements RuleRepository  {

    Map<String,Rule> rules;

    public RuleRepositoryImpl() {
        this.rules = new HashMap<>();
    }

    @Override
    public Optional<Rule> getRule(String sku) {
        if (rules.containsKey(sku)) {
            return Optional.of(rules.get(sku));
        } else {
            return Optional.empty();
        }
    }

    public void load(String csvFile) throws RepositoryException {

        try (Stream<String> stream = Files.lines(Paths.get(csvFile))) {

            // skip first line assuming it has the headers
            stream.skip(1).forEach(line -> {
                String[] fields = line.split(",");
                if (fields.length == 3) {
                    rules.put(fields[0], new Rule(Integer.valueOf(fields[1]), new BigDecimal(fields[2])));
                }
            });

        } catch (IOException | UncheckedIOException | NumberFormatException e) {
            // should log the actual error here
            throw new RepositoryException("Error loading items csv file");
        }
    }

}
