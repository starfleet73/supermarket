package com.itv.supermarket.repository;

import com.itv.supermarket.repository.exception.RepositoryException;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class ItemRepositoryImpl implements ItemRepository {

    private Map<String, BigDecimal> items;

    public ItemRepositoryImpl() {
        this.items = new HashMap<>();
    }

    @Override
    public Optional<BigDecimal> getItemPrice(String sku) {
        if (items.containsKey(sku)) {
            return Optional.of(items.get(sku));
        } else {
            return Optional.empty();
        }
    }

    public void load(String csvFile) throws RepositoryException {

        try (Stream<String> stream = Files.lines(Paths.get(csvFile))) {

            // skip first line assuming it has the headers
            stream.skip(1).forEach(line -> {
                String[] fields = line.split(",");
                if (fields.length == 2) {
                    items.put(fields[0], new BigDecimal(fields[1]));
                }
            });

        } catch (IOException | UncheckedIOException | NumberFormatException e) {
            // should log the actual error here
            throw new RepositoryException("Error loading items csv file");
        }
    }

}
