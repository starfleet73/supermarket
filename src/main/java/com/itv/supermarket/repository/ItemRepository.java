package com.itv.supermarket.repository;

import java.math.BigDecimal;
import java.util.Optional;

public interface ItemRepository {
    Optional<BigDecimal> getItemPrice(String sku);
}
