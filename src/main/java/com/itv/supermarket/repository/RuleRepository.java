package com.itv.supermarket.repository;

import com.itv.supermarket.model.Rule;

import java.util.Optional;

public interface RuleRepository {
    Optional<Rule> getRule(String sku);
}
