package com.itv.supermarket.service;

import com.itv.supermarket.model.ReceiptItem;
import com.itv.supermarket.model.Rule;
import com.itv.supermarket.repository.ItemRepository;
import com.itv.supermarket.repository.RuleRepository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ShoppingCartServiceImpl implements ShoppingCartService {

    private ItemRepository itemRepository;
    private RuleRepository ruleRepository;

    private List<String> cart;

    public ShoppingCartServiceImpl(ItemRepository itemRepository, RuleRepository ruleRepository) {
        this.itemRepository = itemRepository;
        this.ruleRepository = ruleRepository;
        this.cart = new ArrayList<>();
    }

    @Override
    public void addToCart(String sku) {
        if (itemRepository.getItemPrice(sku).isPresent()) {
            cart.add(sku);
        }
    }

    @Override
    public void emptyCart() {
        cart.clear();
    }

    @Override
    public List<ReceiptItem> checkout() {
        return processRules(processCart());
    }

    private List<ReceiptItem> processCart() {
        List<ReceiptItem> receiptItems = new ArrayList<>();

        cart.stream()
                .collect(Collectors.groupingBy(sku -> sku))
                .forEach((sku, skuGroup) -> {
                    Optional<BigDecimal> itemPrice = itemRepository.getItemPrice(sku);
                    if (itemPrice.isPresent()) {
                        int numberOfItems = skuGroup.size();
                        receiptItems.add(new ReceiptItem(sku, numberOfItems, itemPrice.get().multiply(BigDecimal.valueOf(numberOfItems)), BigDecimal.ZERO));
                    }

                });

        return receiptItems;
    }

    private List<ReceiptItem> processRules(List<ReceiptItem> receiptItems) {
        List<ReceiptItem> processedReceiptItems = new ArrayList<>();
        for (ReceiptItem receiptItem : receiptItems) {
            Optional<Rule> ruleForSku = ruleRepository.getRule(receiptItem.getSku());
            if (ruleForSku.isPresent()) {
                Rule rule = ruleForSku.get();
                BigDecimal itemPrice = itemRepository.getItemPrice(receiptItem.getSku()).orElse(BigDecimal.ZERO);

                int numberOfTimesRuleApplies = receiptItem.getQty() / rule.getQty();

                BigDecimal receiptPrice = receiptItem.getItemPrice()
                        .subtract(itemPrice.multiply(BigDecimal.valueOf(numberOfTimesRuleApplies * rule.getQty() )))
                        .add(rule.getSpecialPrice().multiply(BigDecimal.valueOf(numberOfTimesRuleApplies)));

                processedReceiptItems.add(
                        new ReceiptItem(receiptItem.getSku(), receiptItem.getQty(), receiptItem.getItemPrice(), receiptPrice)
                );

            } else {
                processedReceiptItems.add(
                        new ReceiptItem(receiptItem.getSku(), receiptItem.getQty(), receiptItem.getItemPrice(), receiptItem.getItemPrice())
                );
            }

        }

        // order receipt items by sku
        return processedReceiptItems.stream()
                .sorted(Comparator.comparing(ReceiptItem::getSku))
                .collect(Collectors.toList());
    }

}
