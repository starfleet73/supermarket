package com.itv.supermarket.service;

import com.itv.supermarket.model.ReceiptItem;

import java.util.List;

public interface ShoppingCartService {
    void addToCart(String sku);
    void emptyCart();
    List<ReceiptItem> checkout();
}
