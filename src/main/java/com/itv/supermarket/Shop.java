package com.itv.supermarket;

import com.itv.supermarket.model.ReceiptItem;
import com.itv.supermarket.repository.ItemRepositoryImpl;
import com.itv.supermarket.repository.RuleRepositoryImpl;
import com.itv.supermarket.service.ShoppingCartService;
import com.itv.supermarket.service.ShoppingCartServiceImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

public class Shop {

    public static void main(String[] args) {

        if (args.length ==2) {
            ItemRepositoryImpl itemRepository = new ItemRepositoryImpl();
            RuleRepositoryImpl ruleRepository = new RuleRepositoryImpl();

            try {
                itemRepository.load(args[0]);
                ruleRepository.load(args[1]);

                goShopping(new ShoppingCartServiceImpl(itemRepository, ruleRepository));

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("invalid arguments - [item_prices.csv] [rule_special_price.csv]");
        }
    }

    private static void goShopping(ShoppingCartService shoppingCartService) {
        System.out.println("******************************************************************");
        System.out.println("* ITV Supermarket                                                *");
        System.out.println("******************************************************************");
        System.out.println("- To shop enter sku letters separated by a space and press [enter]");
        System.out.println("- Type 'quit' to exit");
        System.out.println("- Example: A B C [enter]");

        boolean shopping = true;
        Scanner scanner = new Scanner(System.in);
        while (shopping) {
            String tolley = scanner.nextLine();
            if ("quit".equals(tolley)) {
                shopping = false;
            } else {
                String[] items = tolley.split(" ");
                for (String item : items) {
                    shoppingCartService.addToCart(item.toUpperCase());
                }
                printReceipt(shoppingCartService.checkout());
                shoppingCartService.emptyCart();
            }

        }

    }

    private static void printReceipt(List<ReceiptItem> receiptItems) {
        System.out.println("-----------------------------------------");
        System.out.println("Shopping Receipt");
        System.out.println("-----------------------------------------");
        System.out.println("SKU\tQTY\tPRICE\tRECEIPT PRICE");
        BigDecimal total = BigDecimal.ZERO;
        for (ReceiptItem receiptItem : receiptItems) {
            System.out.printf("%s\t%d\t£%.2f\t£%.2f\n",
                    receiptItem.getSku(), receiptItem.getQty(), receiptItem.getItemPrice(), receiptItem.getReceiptPrice());
            total = total.add(receiptItem.getReceiptPrice());
        }
        System.out.println("-----------------------------------------");
        System.out.printf("Total\t\t\t£%.2f\n", total);
        System.out.println("-----------------------------------------");
    }

}
